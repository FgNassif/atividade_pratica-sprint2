import { Router } from "express"
import { MessagesControler } from "./cotrollers/MessagesController";
import { SettingsCotrollers } from "./cotrollers/SettingsController";
import { UsersController } from "./cotrollers/UsersController";


const routes = Router();
const settingsCotroller = new SettingsCotrollers();
const usersController = new UsersController();
const messagesControler = new MessagesControler();

routes.post("/settings", settingsCotroller.create);
routes.get("/settings/:username", settingsCotroller.findByUsername);
routes.put("/settings/:username", settingsCotroller.update);

routes.post("/users", usersController.create);
routes.post("/messages", messagesControler.create);
routes.get("/messages/:id", messagesControler.showByUser);



export{ routes };