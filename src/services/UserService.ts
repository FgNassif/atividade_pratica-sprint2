import { getCustomRepository, Repository } from "typeorm"
import { User } from "../entities/User";
import { UserRepository } from "../repositories/UsersRepository"


/**
 * Verifica se o usuário existe.
 * Se não existir, salva no Banco de dados
 * Se existir retorna o user
 */


class UserService {
    private usersRepository: Repository<User>
    constructor(){
        this.usersRepository = getCustomRepository(UserRepository);
    }

    async create(email: string) {

        //Verifica se o usuário existe
        const userExists = await this.usersRepository.findOne({
            email,
        })

        //Se existir retorna o usuário
        if(userExists){
            return userExists;
        }

        const user = this.usersRepository.create({
            email,
        });

        await this.usersRepository.save(user)
            
        return user;
    }

    async findByEmail(email: string) {
        const user = await this.usersRepository.findOne({
          email,
        });
        return user;
      }

}

export {UserService};