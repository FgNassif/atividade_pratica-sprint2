<h1> Atividade prática </h1>

<p> Este é um projeto da criação de um chat para atendimento online. Para garantir o funcionamento do projeto foram utilizadas as tecnologias: Node.js, typescript, Express, Socket.io .</p>

<p> No desenvolvimento desse projeto foi criado um banco de dados que permite a comunicação de administrador com cada um dos usuários do chat, possibilitando o atendimento a muitas pessoas ao mesmo tempo. No acesso o usuário deve inserir a mensagem e o email e então o administrador pode abrir o chat com o usuário e iniciar a comunicação. Caso o usuário ja tenha inserido seus dados anteriormente, um histório de conversa recupera o chat anterior. </p>


<h2> Para executar o projeto siga os seguintes passos: </h2>

- Instale a pasta node_modules utilizando `npm i` ou `yarn i` no terminal <br>
- Inicie o servidor utilizando `npm dev` ou `yarn dev <br>
- O código estará disponível em `localhost:3333` <br>




